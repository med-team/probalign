CXX = g++
CXXFLAGS = -O3 -W -Wall -funroll-loops
TARGET = probalign

all : $(TARGET)

probalign : MultiSequence.h Sequence.h FileBuffer.h SparseMatrix.h EvolutionaryTree.h SafeVector.h Main.cc Sequence.h PostProbs.cc ComputeAlignment.cc ReadMatrix.cc Matrix.h
	$(CXX) $(CXXFLAGS) -lm -o $(TARGET) Main.cc PostProbs.cc ComputeAlignment.cc ReadMatrix.cc

clean: 
	rm -f $(TARGET)
